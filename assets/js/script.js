$(document).ready(function () {
  function error() {
    $("#error-message")
      .show()
      .addClass("slide-in-top")
      .removeClass("slide-out-top");
    setTimeout(() => {
      $("#error-message").addClass("slide-out-top").removeClass("slide-in-top");
    }, 2000);
  }

  function success() {
    $("#success-message")
      .show()
      .addClass("slide-in-top")
      .removeClass("slide-out-top");
    setTimeout(() => {
      $("#success-message")
        .addClass("slide-out-top")
        .removeClass("slide-in-top");
    }, 2000);
  }

  let typingTimer;
  let doneTypingInterval = 2000;
  let artistNameFilter = document.getElementById("artistName-filter");

  artistNameFilter.addEventListener("keyup", () => {
    clearTimeout(typingTimer);
    typingTimer = setTimeout(doneTyping, doneTypingInterval);
  });

  function doneTyping() {
    let searchTxt = $("#artistName-filter").val();
    searchTxt = searchTxt.replace(/[.()+]/g, "\\$&");

    let patt = new RegExp(searchTxt, "i");

    $(".artist-name").each(function () {
      if (patt.test($(this).val())) {
        $(this).closest("label").slideDown(500);
      } else $(this).closest("label").slideUp(500);
    });
  }

  let tagNameFilter = document.getElementById("tagName-filter");

  tagNameFilter.addEventListener("keyup", () => {
    clearTimeout(typingTimer);
    typingTimer = setTimeout(tagDoneTyping, doneTypingInterval);
  });

  function tagDoneTyping() {
    let searchTxt = $("#tagName-filter").val();
    searchTxt = searchTxt.replace(/[.()+]/g, "\\$&");

    let patt = new RegExp(searchTxt, "i");

    $(".tag-name").each(function () {
      if (patt.test($(this).val())) {
        $(this).closest("label").slideDown(500);
      } else $(this).closest("label").slideUp(500);
    });
  }

  $("#coverImg").on("change", function () {
    var files = !!this.files ? this.files : [];
    if (!files.length || !window.FileReader) return;
    if (/^image/.test(files[0].type)) {
      var reader = new FileReader();
      reader.readAsDataURL(files[0]);
      reader.onloadend = function () {
        $(".coverImg").css("background-image", "url(" + this.result + ")");
      };
    }
  });

  var platforms = [];

  $(document).on("click", ".platform-name", function (e) {
    e.preventDefault();
    let dataId = $(this).data("id");
    let dataLabel = $(this).data("label");
    let dataSubmition = $(this).data("submition");

    if ($(this).children("input")[0].checked) {
      var PlatformsVal = $(this).children("input")[0].value;

      var newPlatforms = jQuery.grep(platforms, function (value) {
        return value != PlatformsVal;
      });
      platforms = newPlatforms;

      function removeItem(arr, item) {
        return arr.filter((f) => f !== item);
      }

      platforms = removeItem(platforms, $(`.${dataId}:checked`).val());

      $("#platforms").val(platforms);
      $(`#${dataId}`).remove();
      $(this).children("input").prop("checked", false);
    } else {
      $(this).children("input").prop("checked", true);

      $("#newMusic-platform")
        .append(`<div id="${dataId}" class="platform-timing margin-top-20">
          <p class="label">
            زمان ارسال آهنگ به ${dataLabel}
          </p>
          <div class="input-holder">
            <div class="checkbox-container">
              <label class="container immediate-submission"
                >ارسال فوری
                <input
                  class="${dataId}"
                  value="${dataLabel}:ارسال فوری"
                  type="radio"
                  name="${dataSubmition}-submission"
                />
                <span class="checkmark rounded"></span>
              </label>
              <label class="container schedule-submission"
                >ارسال زمانبندی
                <input
                  class="${dataId}"
                  value="${dataLabel}:ارسال زمانبندی"
                  type="radio"
                  name="${dataSubmition}-submission"
                />
                <span class="checkmark rounded"></span>
              </label>
            </div>
          </div>
         
        </div><br>`);

      let iminput = $(`.immediate-submission .${dataId}`).val();
      $(`.immediate-submission .${dataId}`).prop("checked", true);
      platforms.push(iminput);
      $("#platforms").val(platforms);
      console.log($("#platforms").val());
    }
  });

  $("body").on("click", ".schedule-submission", function () {
    var PlatformsVal = $(this).siblings().children("input")[0].value;

    function removeA(arr) {
      var what,
        a = arguments,
        L = a.length,
        ax;
      while (L > 1 && arr.length) {
        what = a[--L];
        while ((ax = arr.indexOf(what)) !== -1) {
          arr.splice(ax, 1);
        }
      }
      return arr;
    }
    removeA(platforms, PlatformsVal);

    if (!$(this).children("input")[0].checked) {
      platforms.push(`${$(this).children("input")[0].value}`);
      $("#platforms").val(platforms);
    } else {
      var newPlatforms = jQuery.grep(platforms, function (value) {
        return value != PlatformsVal;
      });
      platforms = newPlatforms;
      $("#platforms").val(platforms);
    }
    console.log($("#platforms").val());
  });

  $("body").on("click", ".immediate-submission", function () {
    var PlatformsVall = $(this).siblings().children("input")[0].value;

    function removeA(arr) {
      var what,
        a = arguments,
        L = a.length,
        ax;
      while (L > 1 && arr.length) {
        what = a[--L];
        while ((ax = arr.indexOf(what)) !== -1) {
          arr.splice(ax, 1);
        }
      }
      return arr;
    }

    removeA(platforms, PlatformsVall);

    if (!$(this).children("input")[0].checked) {
      platforms.push(`${$(this).children("input")[0].value}`);
      $("#platforms").val(platforms);
    } else {
      var newPlatformss = jQuery.grep(platforms, function (value) {
        return value != PlatformsVall;
      });
      platforms = newPlatformss;
      $("#platforms").val(platforms);
    }
    console.log($("#platforms").val());
  });

  $(".category-name").change(function () {
    if (this.checked && this.value == "به زودی") {
      $(".category-name")
        .filter(function () {
          return this.value === "آهنگ جدید";
        })
        .prop("checked", true);
    } else if (this.checked && this.value == "فروشی") {
      $("#file320 , .file320").addClass("d-none");
      $("#purchaseLink").removeClass("d-none");
      $(".category-name")
        .filter(function () {
          return this.value === "آهنگ جدید";
        })
        .prop("checked", true);
    } else if (!this.checked && this.value == "فروشی") {
      $("#file320 , .file320").removeClass("d-none");
      $(".upload-files").removeClass("flex-column");
      $("#purchaseLink").addClass("d-none");
    }
  });

  $("#file320").change(function () {
    let fileLabel = $(this).attr("name");
    $("#success-message").text("فایل 320 اضافه شد");
    success();
    $(`.${fileLabel}`)
      .children("img")
      .removeClass("d-none")
      .addClass("scale-in-center");
  });

  $("#videoFile").change(function () {
    let fileLabel = $(this).attr("name");
    $("#success-message").text("فایل ویدئو اضافه شد");
    success();
    $(`.${fileLabel}`)
      .children("img")
      .removeClass("d-none")
      .addClass("scale-in-center");
  });

  // add new artist func

  $("#add-artist").click(function () {
    let newArtistFa = $("#newArtist-name-fa").val();
    let newArtistEn = $("#newArtist-name-en").val();
    if (newArtistFa && newArtistEn) {
      $(".newMusic-artist .checkbox-container").prepend(
        `<label class="container">${newArtistFa}
      <input value="${newArtistFa}" class="artist-name" type="checkbox">
      <span class="checkmark"></span>
    </label>`
      );
      $("#newArtist-name-fa, #newArtist-name-en").val("");
      $("#success-message").text("خواننده جدید اضافه شد");
      success();
    } else if (!newArtistFa) {
      $("#error-message").text("لطفا نام خواننده را به فارسی وارد نمایید");
      error();
    } else if (!newArtistEn) {
      $("#error-message").text("لطفا نام خواننده را به انگلیسی وارد نمایید");
      error();
    }
  });

  // add new tag func

  $("#add-tag").click(function () {
    let newTagFa = $("#newTag-name-fa").val();
    if (newTagFa) {
      $(".newMusic-tag .checkbox-container").prepend(
        `<label class="container">${newTagFa}
      <input value="${newTagFa}" class="tag-name" type="checkbox">
      <span class="checkmark"></span>
    </label>`
      );
      $("#newTag-name-fa").val("");
      $("#success-message").text("تگ جدید اضافه شد");
      success();
    } else {
      $("#error-message").text("لطفا نام تگ را وارد نمایید");
      error();
    }
  });

  // $("#newArtist-name-fa, #newTag-name-fa").bind(
  //   "keypress paste input",
  //   function (e) {
  //     var regex = new RegExp("^[a-zA-Z0-9]+$");
  //     var str = String.fromCharCode(!e.charCode ? e.which : e.charCode);
  //     if (regex.test(str)) {
  //       e.preventDefault();
  //       $("#error-message").text("زبان کیبورد خود را فارسی کنید");
  //       error();
  //     }
  //     return true;
  //   }
  // );

  $("#newArtist-name-en, #newMusic-name-en").on("keypress", function (event) {
    var ew = event.which;
    if (ew == 32) return true;
    if (48 <= ew && ew <= 57) return true;
    if (65 <= ew && ew <= 90) return true;
    if (97 <= ew && ew <= 122) {
      return true;
    } else {
      event.preventDefault();
      $("#error-message").text("زبان کیبورد خود را انگلیسی کنید");
      error();
      return false;
    }
  });

  $("#newArtist-name-fa, #newTag-name-fa, #newMusic-name-fa").on(
    "keypress",
    function (event) {
      var ew = event.which;
      if (ew == 32) {
        return true;
      }
      if (48 <= ew && ew <= 57) {
        event.preventDefault();
        $("#error-message").text("زبان کیبورد خود را فارسی کنید");
        error();
        return false;
      }
      if (65 <= ew && ew <= 90) {
        event.preventDefault();
        $("#error-message").text("زبان کیبورد خود را فارسی کنید");
        error();
        return false;
      }
      if (97 <= ew && ew <= 122) {
        event.preventDefault();
        $("#error-message").text("زبان کیبورد خود را فارسی کنید");
        error();
        return false;
      } else {
        return true;
      }
    }
  );

  // $("#newArtist-name-en").bind("keypress paste input", function (e) {
  //   var regex = new RegExp("^[a-zA-Z0-9]+$");
  //   var str = String.fromCharCode(e.charCode ? e.which : e.charCode);
  //   if (regex.test(str)) {
  //     return true;
  //   } else {
  //     e.preventDefault();
  //     $("#error-message").text("زبان کیبورد خود را انگلیسی کنید");
  //     error();
  //   }
  // });

  // loading tag - artist - category func

  var artists = [];
  var category = [];
  var tag = [];

  $(function () {
    $("body").on("click", ".artist-name", function () {
      if (this.checked) {
        artists.push(`${this.value}`);
        $("#chosen-artists").val(artists);
      } else {
        var artistVal = this.value;

        var newArtists = jQuery.grep(artists, function (value) {
          return value != artistVal;
        });
        artists = newArtists;
        $("#chosen-artists").val(artists);
      }
      console.log(artists);
    });
  });

  $(function () {
    $("body").on("click", ".category-name", function () {
      if (this.checked) {
        category.push(`${this.value}`);
        if (
          (this.value === "فروشی" || this.value === "به زودی") &&
          $.inArray("آهنگ جدید", category) == -1
        ) {
          category.push(`آهنگ جدید`);
        }
        $("#chosen-category").val(category);
      } else {
        var categoryVal = this.value;
        var newCategory = jQuery.grep(category, function (value) {
          return value != categoryVal;
        });
        category = newCategory;
        $("#chosen-category").val(category);
      }
      console.log(category);
    });
  });

  $(function () {
    $("body").on("click", ".tag-name", function () {
      if (this.checked) {
        tag.push(`${this.value}`);
        $("#chosen-tag").val(tag);
      } else {
        var tagVal = this.value;

        var newTag = jQuery.grep(tag, function (value) {
          return value != tagVal;
        });
        tag = newTag;
        $("#chosen-tag").val(tag);
      }
      console.log(tag);
    });
  });

  // edit platform

  $(".platform-edit").click(function () {
    let platformEdit = $(this).attr("data-name");
    console.log(platformEdit);

    //ajax func here
  });

  // validation
  $("#newMusic-submit").click(function (e) {
    e.preventDefault();
    let musicNameFa = $("#newMusic-name-fa").val();
    let musicNameEn = $("#newMusic-name-en").val();
    let musicArtist = $("#chosen-artists").val();
    let musicCategory = $("#chosen-category").val();
    let musicCover = $("#coverImg").val();
    let musicPlatform = false;

    if ($(".platform-name input").is(":checked")) {
      musicPlatform = true;
    } else if ($(".platform-name input").is(":not(:checked)")) {
      musicPlatform = false;
    }

    if (
      musicNameFa &&
      musicNameEn &&
      musicArtist &&
      musicCategory &&
      musicCover &&
      musicPlatform
    ) {
      // $("#addNewMusic-form").submit();
      $("#uploadLoader").css("display", "flex");
      $("#uploadLoader h1, #uploadLoader svg")
        .show()
        .addClass("scale-in-center");
    } else {
      if (!musicNameFa) {
        $("#error-message").text("لطفا نام فارسی آهنگ را وارد کنید");
        error();
      } else if (!musicNameEn) {
        $("#error-message").text("لطفا نام انگلیسی آهنگ را وارد کنید");
        error();
      } else if (!musicArtist) {
        $("#error-message").text("لطفا خواننده آهنگ را انتخاب کنید");
        error();
      } else if (!musicCategory) {
        $("#error-message").text("لطفا دسته‌بندی آهنگ را انتخاب کنید");
        error();
      } else if (!musicCover) {
        $("#error-message").text("لطفا کاور آهنگ را انتخاب کنید");
        error();
      } else if (!musicPlatform) {
        $("#error-message").text("لطفا پلتفرم آهنگ را انتخاب کنید");
        error();
      }
    }
  });
});
